# ARA

Athena ROOT access for doing quick EVNT based analysis. Please setup an athena release before launching the code

## Working setup 
lsetup "asetup 19.2.5.33,AtlasProduction"

python rootAccess.py input.txt 

input.txt is a comma separated file which contains the input EVNT files

## Running on the Grid 
First do lsetup panda

check the runSub.sh script and add/comment out files you want to submit. Then simply execute the runSub.sh script 