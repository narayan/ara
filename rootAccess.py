import user
import sys 
import ROOT
import csv
#import PyCintex
import AthenaROOTAccess.transientTree

def loop(begin, end):
    """Convert a pair of C++ iterators into a python generator"""
    while (begin != end):
        yield begin.__deref__()  #*b
        begin.__preinc__()       #++b 

def getDaughters(part):
    decayVtx = part.end_vertex()
    if decayVtx:
        pOut_beg = decayVtx.particles_out_const_begin()
        pOut_end = decayVtx.particles_out_const_end()
        return [x for x in loop(pOut_beg,pOut_end)]
    else:
        return list()
def getParents(part):
    prodVtx = part.production_vertex()
    if prodVtx:
        pIn_beg     = prodVtx.particles_in_const_begin()
        pIn_end     = prodVtx.particles_in_const_end()
        return [x for x in loop(pIn_beg,pIn_end)]
    else:
        return list()

def getSiblings(part):
    prodVtx = part.production_vertex()
    if prodVtx:
        pIn_beg     = prodVtx.particles_out_const_begin()
        pIn_end     = prodVtx.particles_out_const_end()
        return [x for x in loop(pIn_beg,pIn_end)]
    else:
        return list()

#In case a particle reshuffles energy/momentum in every step in hepMC record it may look like cascade with same PDG_id 
# eg top->top + gamma
#           |-> top -> top 
# the above is in inprinciple possible. This recursive function should get the last particle in such chains. 
# WARNING: NOT tested properly

def getLastInChain(part):
    if len([x for x in getDaughters(part) if x.pdg_id() == part.pdg_id()])==0 :
        return part
    else:
        for x in getDaughters(part):
            if x.pdg_id() == part.pdg_id():
                getLastInChain(x)

def getFirstInChain(part):
    if len([x for x in getParents(part) if x.pdg_id() == part.pdg_id()])==0:
        return part
    else:
        for x in getParents(part):
            if x.pdg_id() == part.pdg_id():
                getFirstInChain(x)



## The topology selection goes in here
##Modify ,duplicate or overload this funciton as needed
def fillSelection(beg,end,dghterPDG,brnchVar,treOut):
    cand_topo = [] #event recod decay topology candidate
    for part in loop(beg,end):
        if ((abs(part.pdg_id())==dghterPDG[0] or abs(part.pdg_id())==dghterPDG[1])):
            prodVtx     = part.production_vertex()
            if prodVtx:
                pOut_beg    = prodVtx.particles_out_const_begin()
                pIn_beg     = prodVtx.particles_in_const_begin()
                pOut_end    = prodVtx.particles_out_const_end()
                pIn_end     = prodVtx.particles_in_const_end()

                daughterpair=()
                for sibling in loop(pOut_beg,pOut_end):
                    if ((abs(sibling.pdg_id()) == dghterPDG[0]and abs(part.pdg_id())==dghterPDG[1]) or (abs(sibling.pdg_id()) ==dghterPDG[0] and abs(part.pdg_id())==dghterPDG[1])):
                        daughterpair = (part,sibling)

                if daughterpair:
                    if daughterpair[0] not in sum(cand_topo,()):
                        for parent in loop(pIn_beg,pIn_end):
                            cand_topo.append((parent,daughterpair[0],daughterpair[1]))




#f = ROOT.TFile.Open("/eos/atlas/user/n/narayan/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.EVNT/EVNT.12458444._006021.pool.root.1")
ch=ROOT.AthenaROOTAccess.TChainROOTAccess('CollectionTree')
with open(sys.argv[1]) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for li in csv_reader:
            for smpl in li: 
                ch.AddFile(smpl)

#f = ROOT.TFile.Open(sys.argv[1])

#tt = AthenaROOTAccess.transientTree.makeTree(f)
tt = AthenaROOTAccess.transientTree.makeTree(ch)


fOut    = ROOT.TFile("output.root","RECREATE")
treOut  = ROOT.TTree("selection","photon to lepton selection")


#Define your vector branch variables. Also do not forget to "clear" the vector at the begninning of the event looping
from array import array 



EventNumber  = array('L',[0])
weight       = array('d',[0])
parentStatus =  ROOT.std.vector(ROOT.Long)()
parentPDG    =  ROOT.std.vector(ROOT.Long)()
lepPairMass  =  ROOT.std.vector(ROOT.Double)()
lepPairPt    =  ROOT.std.vector(ROOT.Double)()
lepPairDr    =  ROOT.std.vector(ROOT.Double)()
lepPairDEta  =  ROOT.std.vector(ROOT.Double)()
lep1id       =  ROOT.std.vector(ROOT.Long)()
lep1pt       =  ROOT.std.vector(ROOT.Double)()
lep1eta      =  ROOT.std.vector(ROOT.Double)()
lep1phi      =  ROOT.std.vector(ROOT.Double)()
lep1E        =  ROOT.std.vector(ROOT.Double)()
lep2id       =  ROOT.std.vector(ROOT.Long)()
lep2pt       =  ROOT.std.vector(ROOT.Double)()
lep2eta      =  ROOT.std.vector(ROOT.Double)()
lep2phi      =  ROOT.std.vector(ROOT.Double)()
lep2E        =  ROOT.std.vector(ROOT.Double)()
topMass      =  ROOT.std.vector(ROOT.Double)()
topStarMass  =  ROOT.std.vector(ROOT.Double)()


# Register them as branches

treOut.Branch("EventNumber",EventNumber,"EventNumber/I")
treOut.Branch("weight",weight,"weight/D")
treOut.Branch("parentStatus",parentStatus)
treOut.Branch("parentPDG",parentPDG)
treOut.Branch("lep1id",lep1id)
treOut.Branch("lep1pt",lep1pt)
treOut.Branch("lep1eta",lep1eta)
treOut.Branch("lep1phi",lep1phi)
treOut.Branch("lep1E",lep1E)
treOut.Branch("lep2id",lep2id)
treOut.Branch("lep2pt",lep2pt)
treOut.Branch("lep2eta",lep2eta)
treOut.Branch("lep2phi",lep2phi)
treOut.Branch("lep2E",lep2E)
treOut.Branch("topMass",topMass)
treOut.Branch("topStarMass",topStarMass)
treOut.Branch("lepPairMass",lepPairMass)
treOut.Branch("lepPairPt",lepPairPt)
treOut.Branch("lepPairDr",lepPairDr)
treOut.Branch("lepPairDEta",lepPairDEta)

for iev in tt:
    evt = iev.GEN_EVENT[0]
    EventNumber[0] = iev.McEventInfo.event_ID().event_number()
    runNumber = iev.McEventInfo.event_ID().run_number()
    weight[0]   = evt.weights().front()
    beg = evt.particles_begin()
    end = evt.particles_end()
    selPair = []
    #print "EventNumber: ",EventNumber[0]
    cand_topo = []
    parentStatus.clear()
    parentPDG.clear()
    lep1id.clear()
    lep1pt.clear()
    lep1eta.clear()
    lep1phi.clear()
    lep1E.clear()
    lep2id.clear()
    lep2pt.clear()
    lep2eta.clear()
    lep2phi.clear()
    lep2E.clear()
    topMass.clear()
    topStarMass.clear()
    lepPairMass.clear()
    lepPairPt.clear()
    lepPairDr.clear()
    lepPairDEta.clear()
    
    fillSelection(beg,end,[24,5],topMass,treOut)
    fillSelection(beg,end,[6,22],topStarMass,treOut)

    treOut.Fill()


treOut.Write()
fOut.Close()
